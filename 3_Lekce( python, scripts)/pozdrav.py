
# coding: utf-8

"""Tento script se zepta na jmeno a pozdravi
a secte 2 cisla"""
print("Prave jsi sputil python skript!")

def pozdrav(jmeno):
    """Funkce pozdrav"""
    msg = "Hello, {jm}!".format(jm=jmeno)
    print(msg)

    
def main():
    """Hlavni funkce programu"""
    
    jmeno = input("Zadej jmeno:")
    pozdrav(jmeno)
    
    cislo_a = int(input("Zadej cislo: "))
    cislo_b = int(input("Zadej cislo2> "))
    
    vysledek = secti(cislo_a, cislo_b)
    
    msg = "Vysledek souctu {a} + {b} = {vysledek}".format(a=cislo_a,
                                                          b=cislo_b,
                                                          vysledek=vysledek)
    print(msg)
    
def secti(a, b):
    """Secte 2 cisla a vrati vysledek"""
    return a + b
    
if __name__ == "__main__":
    main()


