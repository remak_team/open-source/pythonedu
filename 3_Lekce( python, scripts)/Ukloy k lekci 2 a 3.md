Ukoly k lekci 2 a 3
Vstupni data pro nektere ulohy najdete v prilozenem notebook, zkopirujte si je prosim do sveho a pouzijte.

1. Prevedte nasledujici text na znaku => "['50','40','-35']" `'50|-32|-38|25|24|-13|-33|-39|16|3|10|49|44|24|-27|50|42|-5|13|-47|41|13|18|49|-11|23|-22|-7|9|-38'`

2. Nahradte rozdilne separatory jednim  `9,-21;-29,-28;-19,32,-9,-25;-2,7,7;37;-10,-42,-22,41,-23;27;15;38,25,28;-44;3,4,34;45;45,33;34`

3. ![obr](/images/magical_formula.jpg) z nasledujiciho obrazku vypocitejte hodnotu **P<sub>1<sub>** jako vstup vyuzijte tyto hodnoty (si napsat funkci abyste ji mohli pouzit vicekrat na jina cisla)  
**D<sub>0</sub>** = 2.25,  
**P<sub>0</sub>** = 101.325,  
**x** = 0.106   
**vysledek je zhruba** 20640  

4. zkuste vypocitat zpet hodnotu **x**, druhou odmocninu najdete v modulu **math** (suqre root - sqrt)

5. vytvorte list, ktery bude nahodne delky mezi 10 - 50 prvky a bude obsahovat nahodna cisla od 0-10(krom)

6. Napiste funkci, ktera splnuje: 
<dl>
  <dt>Vstup</dt>
  <dd>libovolny: <b>list</b>, <b>tuple</b>, <b>string</b>, <b>int</b>, <b>float</b></dd>
  <dt>Vystup</dt>
</dl>
 pro iterables krom string
 ```
 Datovy typ je <Typ> a hodnoty jsou: <hodnoty>.
 ```
 Pro string
 ```
 Datovy typ je string delky <delka vstupniho textu>
 ```
 Pro cisla
 ```
 Datovy typ je <ciselny typ> a zadane cislo je <vetsi / mensi> nez 10.
 ```
 Pro vse ostatni
 ```
 Datovy typ je: <typ>, a na ten se podivame jindy.
 ```

7. Napiste funkci, ktera bude vracet logickou hodnotu **True** nebo **False** podle toho, zdali je argument
palindromem. Funkce bude umet akceptovat sekvence, ne pouze stringy.
Využijte slicing. [slicing na stack overflow][slicing]

8. Napiste funkci **sqr** pro vypocet druhé mocniny. Pak vytvorte funkci **sqr_lst**, která bude prijímat
jeden parametr, který bude sekvencí. Funkce vrátí seznam, který bude obsahovat prvky
puvodní sekvence umocnené na druhou pomocí funkce **sqr**.

9. Naprogramujte funkci multiply, která bude prijimat **2** argumenty. Prvním je císlo, druhým
pak **list**. Pomoci funkce **map** vratte **list**, ktery bude obsahovat puvodni prvky listu vynásobené
prvním argumentem funkce.

10. Vytvorte funkci **my_map**, ktera bude stejne jako funkce map prijímat dva parametry. Prvním
parametrem je funkce, druhým pak list. Výstup bude stejný jako u funkce map. Pri implementaci
použijte [list comprehension][list_comprehension].  

11. Napiste 2 funkce pro kodovani a dekodovani [caesarovy sifry][caesar_cypher]. Funkce budou prijimat 2 argumenty **1. je cislo**, ktere urci o kolik znaku se posune cely retezec a **2. string**, coz je zadany text, ktery se ma zakodovat nebo dekodovat.  
<sub>budete potrebovat funkce <b>ord</b> a <b>chr</b></sub>  
Funkci pro zakodovani pojmenujte: **caesar_encode**
Funkci pro dekodovani pojmenujte: **caesar_decode**
priklad:
 ```
tajna_zprava = "Leto bude skvele, protoze umime Python!"
# zakodovat
kodovana_zprava = caesar_encode(13, tajna_zprava)
print(kodovana_zprava)
# vysledek kodovani: Yr|-oqr-xryr9-}||r-zvzr-]u|{.
 
# dekodovat
dekodovana_zprava = caesar_decode(13, kodovana_zprava)
print(dekodovana_zprava)
# vysledek dekodovani: Leto bude skvele, protoze umime Python!
 ```
Pokud chcete predesly priklad vylepsit a trochu  vic si zaprogramovat a premyslet, prinutte funkce prijmat pouze tisknutelne znaky,
tak ze budou pouzivat sadu z naku z modulu `string.printable`, (`import string` a puzit `string.printable`, dalsi varianta je importovat pouze funkci nebo tridu z modulu `from string import printable` a pouzivate pak uz jen jako `printable`)


**Uzitecne odkazy**  
* Tiny Python3.6 [Notebook] - uzitecna pomucka plna referenci na python
* Jak ma python kod vypadat: [PEP8]
* Odkaz na skvelou knihu ktera je zdarma a i cesky Dive into python [Ponorme se do pythonu3][dive_into_python_cz]
* Dalsi zdroj uloh najdete treba na [CheckIO.org][checkio] - zde si vytvorite ucet a pak prohledavate ruzne ostrovy s ulohami, zakladni jsou Home a jednodussi je Elementary  
* Pokud vam nektere koncepty nejsou jasne vyuzijte stranku [PyTuthor][pytuthor], kde si staci vybrat Python3.6 vlozit kod a muzete si prohlizet vizualizaci co se deje na pozadi.


[Notebook]: https://github.com/mattharrison/Tiny-Python-3.6-Notebook/blob/master/python.rst
[PEP8]: https://www.python.org/dev/peps/pep-0008/
[slicing]: http://stackoverflow.com/questions/509211/explain-pythons-slice-notation
[list_comprehension]: http://diveintopython3.py.cz/comprehensions.html#listcomprehension
[dive_into_python_cz]: http://diveintopython3.py.cz/index.html
[caesar_cypher]: https://hr.wikipedia.org/wiki/Cezarova_šifra
[checkio]: http://checkio.org
[pytuthor]: http://www.pythontutor.com/visualize.html#mode=edit
