tip: pro otevreni prikazove radky v aktualni slozce drzet **[Shift]** a kliknout pravym tlacitkem mysi: **[Shift] + RMB**
Budu sem psat i linuxove alternativy, protoze jsou v pythonu casto pouzivane.

```
C:\Users\jgazda\Python\Notebooky>
                       ^ <- aktualni adresar
```
**\>** posledni znak se nazyva prompt (v linuxu se meni mezi $ a #), kazdy prikaz ktery zadam bude zacinat promptem (viz nize)

![cmd](./images/explorer_path.png)

navigace je velmi podobna jako klikani skrz slozky, muzete se orientovat podle adresy ve standardnim okne exloreru,
kde znak **\>** oddeluje jednotlive slozky, tak v prikladu vyse jsou jednotlive slozky oddeleny pomoci **\\** (Backslash)

**vypis aktualniho adresare**
prikaz **dir** - v linuxu je to prikaz **ls** znamena list directory
```
C:\Users\jgazda\Python\Notebooky>dir
 Volume in drive C is OS

 Directory of C:\Users\jgazda\Python\Notebooky

02-Mar-17  08:07    <DIR>          .
02-Mar-17  08:07    <DIR>          ..
01-Mar-17  13:47    <DIR>          .ipynb_checkpoints
02-Mar-17  08:07             6,543 .~Python1.lekce.ipynb
02-Mar-17  08:07             6,543 Python1.lekce.ipynb
               2 File(s)         13,086 bytes
               3 Dir(s)  64,703,406,080 bytes free

```
Slozky jsou oznaceny jako \<DIR\> a soubory nijak, vypis rika ze dane slozce jsou 3 slozky
**.** - je aktualni adresar / slozka kde se nachazite
**..** - je rodicovska slozka 

**Zmena adresare**
-zmena adresare se provadi prikazem **cd** - change direcotry
```
C:\Users\jgazda\Python\Notebooky>
                       ^ <- aktualni adresar
                       
``` 
Momentalne se nachazime v adresary notebooky a my chceme o slozku zpet pouzijeme prikaz
```
>cd ..
C:\Users\jgazda\Python>
                 ^ <- aktualni adresar
```
Pokud checme zpet do slozky opet pouzijeme prikaz **cd**
```
>cd Notebooky
C:\Users\jgazda\Python\Notebooky>
                        ^ <- aktualni adresar
```

**Vytvoreni nove slozky**
Prikaz make directory **md**, v linuxu se pouziva alternativa **mkdir**
Vypiseme si obsah slozky
```
C:\Users\jgazda\Python>dir

 Volume in drive C is OS

 Directory of C:\Users\jgazda\Python

02-Mar-17  08:16    <DIR>          .
02-Mar-17  08:16    <DIR>          ..
01-Mar-17  13:44    <DIR>          .ipynb_checkpoints
02-Mar-17  08:07    <DIR>          Notebooky
01-Mar-17  13:36    <DIR>          Python36-32
               0 File(s)              0 bytes
               5 Dir(s)  64,696,807,424 bytes free                            
```
Vytvorime slozku **NovaSlozka** a opet si udelame vypis
```
C:\Users\jgazda\Python>md NovaSlozka
C:\Users\jgazda\Python>dir
 Volume in drive C is OS

 Directory of C:\Users\jgazda\Python

02-Mar-17  08:16    <DIR>          .
02-Mar-17  08:16    <DIR>          ..
01-Mar-17  13:44    <DIR>          .ipynb_checkpoints
02-Mar-17  08:07    <DIR>          Notebooky
02-Mar-17  08:16    <DIR>          NovaSlozka
01-Mar-17  13:36    <DIR>          Python36-32
               0 File(s)              0 bytes
               6 Dir(s)  64,696,807,424 bytes free
```
