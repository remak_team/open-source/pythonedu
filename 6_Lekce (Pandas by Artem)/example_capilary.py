import pandas as pd

_CSV_PATH = r'https://docs.google.com/spreadsheets/d/1gFJUr8ZLqUo9bf-' \
            r'zuub2cFGw8rIeW0foxoeR7A-mbm0/pub?gid=0&single=true&output=csv'


def cena_capilaru_od_tipu(typ_capilaru):
    """popis

    :param typ_capilaru: bla bla
    :return: bla bla 
    """
    _data = pd.read_csv(_CSV_PATH)
    return _data.ix[_data.Type == typ_capilaru, 'Price_CZK'].max()  # Here I am using max just to convert the value
    # into normal float type


print(cena_capilaru_od_tipu('CAP_2M'))
