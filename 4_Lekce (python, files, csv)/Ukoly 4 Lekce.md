#Ukoly k lekci 4

I. Budeme prohledavat soubor `HonzikovaCesta.txt`
1. Zjistete kolikrat se v souboru nachazi slovo 'Honzík'
2. Spocitejte kolik slov ma cely text bez mezer.
3. Zjistete jake je nejcastesi slovo v textu - pro tuto ulohu pouzijte slovnik, pro ukladani jednotlivich slov a jejich poctu, osetrete pripady kdy je slovo na konci vety nebo je kolem neho jiny znak, napr.: `Honzik.`, `,Honzik`, `"a tenhle"`, vse ostani muzeme brat jako ruzna slova.

II. Budeme pracovat se souborem `kilometers.csv` , ktery obsahuje zaznam sluzebnich cest zamestancu.
Soubor obsahuje nasledujici data: `first_name;last_name;email;km;cost_per_km`
`first_name` - Jmeno
`last_name` - Prijmeni
`email` - email
`km` - kilometru najeto
`fuel_liter_cost` - cena paliva za litr

Pro jednoduchost budeme predpokladat, ze cesty byly uskutecneny stejnym autem s konstatni spotrebou 10litru / 100km

1. Pro kazdou cestu vypiste, kolik stala cena za kilometr `price_per_km`
2. Pro kazdou cestu vypiste, celkovou cenu za 1 cesta `trip_cost`
3. Zjistete, ktery zamestanec ujel nevice kilometru
4. Zjistete, ktery zamestanec mel nejdrazsi cesty
5. Pouzijte data z ukolu **2, 3 a 4** a vytvorte novy soubor `trips2016.csv`, do ktereho zapisete vysledky v nasledujicim formatu `first_name;last_name;email;total_km;total_cost;`
  `total_km` - celkovy pocet najetych kilometru pro zamestnance
  `total_cost` - celkova cena za rok
  
**Napovedy:**
a. Pri parsovani csv je kazdy radek souvisly text(**string**), pouzijte str.split() k odeleni dat
b. Pri vypoctu cisel je potreba text nejprve pretypovat na cislo, teprve potom lze pocitat
c. Je vhodne pouzit slovnk (dict) pro ukladani dat zachovani jedinecnosti zamestnance, napr: ve tvaru `jm_prijm: total_km`, `jm_prijm: total_cost`
d. Seradit slovnik podle hodnot klicu muzete pomoci funkce sorted`sorted(dic.items(), key=lambda kv_tuple: kv_tuple[1])`, ktere predate anonymni funkci `lambda` funkci.
e. pro nacitani csv muzete pouzit i modul `csv` a `csv.DictReader`, pripadne i dict writer.