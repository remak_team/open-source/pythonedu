Python Lekce 7
==============

Plan Lekce 7
------------

- nainstalovat vsechny potrebne nastroje pro lekci 7 a nasledujici
- Ukazka prvniho projektu v PyCharm
- Reseni vsech predchozich ukolu

Pycharm, Git
^^^^^^^^^^^^
Vsechno, co budeme v lekcni instalovat

- PyCharm: https://www.jetbrains.com/pycharm/download/
- Git: https://git-scm.com/downloads
- Python: https://www.python.org/downloads/
- visual c++ redistributable 2015: https://www.microsoft.com/en-us/download/details.aspx?id=48145
- Notepad++: https://notepad-plus-plus.org/download/v7.3.3.html
- VScode: https://code.visualstudio.com/download

Pycharm, Git - uzitecne odkazy
------------------------------

 - snad vse o gitu najdete v knize ktera je cesky! https://git-scm.com/book/cs/v2
 - 2 verejne git servery - [GitLab] [gitlab] a [GitHub][github]
 - [interaktivni git tutorial] [git4]
 - velmi uzitecna [Stack Overflow] [so1] stranka o praci se vzdalenym repozitarem
 - velmi hezky zpracovana [Vizualni prirucka [EN]] [git1], autor Mark Lodato
 - [interaktivni git cheat sheet] [git2] 
 - [navod Gitlab generovani SSH klice] [gitlab2]
 - Clanky o tom jak ma vypaday commit komentar:
    - http://chris.beams.io/posts/git-commit/
    - http://alistapart.com/article/the-art-of-the-commit
    - https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message
    - https://github.com/erlang/otp/wiki/writing-good-commit-messages
    - https://wiki.openstack.org/wiki/GitCommitMessages
    - http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
 - GitHub workflow diagram ![github workflow diagram] [git3] 

  [so1]: http://stackoverflow.com/questions/3329943/git-branch-fork-fetch-merge-rebase-and-clone-what-are-the-differences/ "git branch, fork, fetch, merge, rebase and clone, what are the differences?"
  [git1]: http://marklodato.github.io/visual-git-guide/index-en.html "A Visual Git Reference"
  [git2]: http://ndpsoftware.com/git-cheatsheet.html " Git cheat sheet"
  [git3]: http://justinhileman.info/article/git-pretty/git-pretty.png
  [git4]: https://try.github.io/levels/1/challenges/1 "github interactive tutorial"
  [gitlab]: https://gitlab.com "GitLab"
  [github]: https://github.com "GitHub"
  [gitlab2]: https://gitlab.com/help/ssh/README "SHH key generate"
