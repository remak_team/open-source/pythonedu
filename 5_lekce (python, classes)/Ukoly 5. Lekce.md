# Ukoly 5. lekce


- a) Naprogramujte tridu **MorseKeyboard**, ktera bude umet prevadet text do morseovy abecedy a zpet, trida bude obsahovat nasledujici atributy a metody. Trida podporuje velka pismena abecedy, mezera je reprezentovana jako /
  - atribut: **_keys** - podporovane znaky klavesnice, jako zdroj vyuzite nasledujici slovnik: `{"A":".-", "B":"-...", "C":"-.-.", "D":"-..", "E":".",
                    "F":"..-.", "G":"--.", "H":"....", "I":"..", "J":".---",
                    "K":"-.-", "L":".-..", "M":"--", "N":"-.", "O":"---",
                    "P":".--.", "Q":"--.-", "R":".-.", "S":"...", "T":"-",
                    "U":"..-", "V":"...-", "W":".--", "X":"-..-", "Y":"-.--",
                    "Z":"--..", "0":"-----", "1":".----", "2":"..---",
                    "3":"...--", "4":"....-", "5":".....", "6":"-....", "7":"--...",
                    "8":"---..", "9":"----.", " ": " / ",
                    }`
  - property: **keys** - vrati seznam podporovanych znaku
  - metoda: print_keys - vytiskne tabulku znaku a jejich reprezentace v morseove abecede
  - metoda: **encode_character**: vstup je 1 znak **character**, vystup je reprezentace znaku v moresove kodu, metoda je staticka
  - metoda: **decode_character**: vstup je znak **character** v morseove kodu, vystup znak z podporovane klavesnice, metoda je staticka
  - metoda: **encode_sequence**:  vstup je sekvence znaku **sequence**, nepovinny argument **sound** - pokud je True, metoda vrati zvuky misto carek a tecek - tecku nahradte **di**, carku **dah**, zvuky spojujte pomlckou pr: `A = .- = di-dah`, vystup je text v morseove abecede
  - metoda: **decode_sequence**: vstup je text v morseove kodu **morse_sequence**, vystup je text
  
- b) otevrete soubor **Ukoly 5. Lekce.ipynb** v iphthon notebooku, precte si kod ke tridam Car a HybridCar, a pokuste se doplnit metody podle #TODO komentaru
  
