# Shrnuti lekce

- Navstivili jsme domovskou stranku [python.org](python.org).
- ukazali jsme si dokumentacti [docs.python.org](https://docs.python.org)
- Nainstalovali jsme python
- sputili **python** interpreter (python shell) - v tutorialu se nazyva interactive mode
- pouzili jsme **pip** pro instalaci modulu jupyter
- ukazali si **ipython** (vylepseny interpreter)
- sputili jsme **jupyter notebook**
- ukazali jsme si zakladni datove typy, ulozili si par promennych
- a vytiskli jejich hodnoty pomoci funkce `print()`
- ukazali jsme si jak pouzit funkci `help(objekt)`,  ktera nam vypise informace jak dany objekt / funkci pouzit

# Cviceni:

1. Otevrete si novy notebook a pojmenujte si ho svym jmenem
1. Prostudujte si trosku menu help

Nasledujici ulohy muzete vykonavat, kazdou v jedne bunce nebo vse najednou v jedne bunce (necham na Vas)

1. pouzijte funkci `print` pro vytisknuti vaseho jmena
2. vytvorte promennou `cislo_a`, ulozte do ni hodnotu 12
3. vytvorte promennou `cislo_b`, ulozte do ni hodnotu 30
4. vytisknete obe promenne na jeden radek …
`help(print)` vam napovi `print(value, ...,)` tohle znamena ze muzete argumenty funkce odelovat `","` (carkou) a ona vytiskne vse co ji date
5. vytvorte promennou `soucet` do ktere priradite hodnotu souctu promennych `cislo_a` a `cislo_b` (hodnotu si muzete vytisknout)
6. vytorte promennou `jmeno` a ulozte do ni vase jmeno
7. vytvorte promennou `prijmeni` a ulozte do ni vase prijmeni
8. vyzkousejte si co se stane kdyz pouzijete operatory `<`, `>`, `==`, `!=` na promenne `jmeno` a `prijmeni` napr: `jmeno == primeni` atd..
9. vytvorte promennou `kosik_ovoce`  typu _list_, obsahujici hodnoty (muzete primo kopirovat a vlozit):  `"jablko", "hruska", "banan", "hruska", "pomeranc", "mandarinka", "kiwi", "banan", "hruska"`
10. zjistete kolik kusu ovoce je ve vasem kosiku ( pouzijte funkci `len()`)
11. zjistete kolikrat je `hruska`; ve vasem kosiku pomoci funkce `count()`
12. zjistete na kterem indexu se nachazi `jablko`;
13. zjistete na kterem indexu se nachazi `kiwi`;
14. ve vasem kosiku vymente `kiwi` za `rajce` (mezikroky si muzte tisknout pomoci funkce `print`, at vidite co se deje)
15. zjistete jak seradit ovoce v kosiku podle abecedy
16. vypiste ovoce ktere je v seznamu alespon jednou
17. zjistete zda se v kosiku nachazi `meloun`;
18. pridejte do na konec kosiku `svestka` -> takto bude kosik nejak vypadat `["jablko", …, "hruska", "svestka"]`
19. pridejte na zacatek kosiku `angrest` -> `["angrest", "jablko", …, "hruska", "svestka"]`

# Uzitecne odkazy:

* [https://docs.python.org/3/tutorial/index.html](https://docs.python.org/3/tutorial/index.html) - python tutorial

* [https://docs.python.org/3/library/stdtypes.html](https://docs.python.org/3/library/stdtypes.html) - vestavene (built-in) datove typy

* [https://docs.python.org/3/contents.html](https://docs.python.org/3/contents.html) - obsah dokumentace

* [https://python.cz](https://python.cz)
